import RPi.GPIO as GPIO #Will connect to our circuit board
import time #Will set our timer
#from sense_hat import SenseHat #I have honestly no clue what this is but found it online for our temperature sensor,
                               #I do not know if it will work for us, I do not think so because I do not know how our sensors are working
'''
os, glob, and are used to import temperature probe kernel module and read its data
'''

import os
import glob
 
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

GPIO.setmode(GPIO.BOARD)
GPIO.setup(13,GPIO.IN) #"Switch on":The Switch should be pressed for the heating element to start.
GPIO.setup(15,GPIO.OUT) #Red LED Relay On
GPIO.setup(19,GPIO.OUT) #Yellow LED Add Cascade Hops
GPIO.setup(21,GPIO.OUT) #Green LED Add Malt Extract & Willamette Hops
GPIO.setup(11,GPIO.OUT) # Relay to enable heating element
#GPIO.setup (6, GPIO.IN) # temperature sensor data input ## this is running on the kernel
 
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
file = device_folder + '/w1_slave'

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        return temp_c
    
def set_relay():
    temperature = read_temp()
    print(temperature)
    if GPIO.input(13) == 0:
        GPIO.cleanup()
    if temperature <= 98:
        GPIO.output(11, 1)
        GPIO.output(15, 1) ## Brandon added this. Red LED is going to be turned on to indicate that the relay is sending 240V.
        print("Heating element enabled")
    else: #temperature > 98:
        GPIO.output(11, 0)
        GPIO.output(15, 0)
        print("Heating element disabled") #####there was an instructions written = true on the line below
    time.sleep(10)
    
def main():
    temperature = read_temp()
    while(temperature < 98):
        set_relay()
#    else:
   #add malt and first batch of hops before first 45 minute boil
    #temperature >= 96:
    GPIO.output(21, 1)
    print("1. Add 6 lb wheat malt syrup") #Will print first step to "Add 6 lb wheat malt syrup"
    print("2. Add Willamette hops") #Will print second step to "Add Willamette hops"
    print("Green LED on %s" % time.ctime()) #Will print "White LED on {and the current time}" which indicates that it reach its temperature ## Brandon changed it to Green.
    print("Please wait 45 minutes...")
    duration = 270 # 45 minutes / 10 second increments
    while duration > 0:
        set_relay()
        duration -= 1
        print (duration/6, " minutes")
                    
        #the operator should add the second hops after 45 min once the Yellow LED turns on ## Brandon changed it to Yellow
    GPIO.output(19, 1)
    print("Yellow LED on %s" % time.ctime()) #Will print "Yellow LED on {and the current time}"
    print("3. Add Cascade hops")
#    time.sleep(3600) #60 minutes timer
    duration = 90 ###Brandon changed this from 600 to 90 which is 15 minutes / 10 second increments
    while duration > 0:
        set_relay()
        duration -= 1
        print (duration/6, " minutes") ###Brandon added this in
    GPIO.output(11, 0)
    print("Boiling 60 minutes complete %s" % time.ctime()) #Will print ""Boiling 60 minutes complete {and the current time}"once the 60 minutes has passed

    GPIO.cleanup()

    
main()

